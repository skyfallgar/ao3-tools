import click

from ao3tools.BookmarksDownloader import BookmarksDownloader

@click.command()
@click.option('--username', prompt="Your username", help='Ao3 username.', type=str, required=True)
@click.option('--password', prompt="Your password", help='Ao3 password.', type=str, required=True)
@click.option('--dl_folder', prompt="Bookmarks folder", default="", help='Folder were the works will be stored.', type=str, required=True)
@click.option('--file_extensions', default=["AZW3", "EPUB", "MOBI", "PDF", "HTML"], help='File extension to download.', type=str, multiple=True, required=False)
@click.option('--bookmarks_status', default=["public", "private", "rec"], help='Type of bookmarks to download.', type=str, multiple=True, required=False)
@click.option('--yaml_architecture', prompt="Bookmark folder architecture (yaml file)", help='yaml file containing the folder organization', type=str, required=False)
def dl_bookmarks(username, password, dl_folder, file_extensions, bookmarks_status, yaml_architecture):
    """Downloads all works in the user's bookmarks"""
    
    bk_dw = BookmarksDownloader(username, password, dl_folder, file_extensions, bookmarks_status, yaml_architecture)
    bk_dw.read_bookmarks()

if __name__ == '__main__':
    dl_bookmarks()