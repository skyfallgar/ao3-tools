import requests
import time
import pandas as pd
import os
import re
import yaml

from bs4 import BeautifulSoup
from unidecode import unidecode
from datetime import datetime
from datetime import date
from csv import writer
from requests.exceptions import ChunkedEncodingError
from yaml.loader import SafeLoader

from ao3tools.URL import ao3URL, ao3LoginURL, ao3WorkURL, ao3SeriesURL, ao3BookmarksURL, ao3DownloadURL
from ao3tools.Variables import RETRY_LATER_DELAY
from ao3tools.Variables import ao3_extensions, ao3_bookmarks_status

from ao3tools.Utils import get_soup, get_lastPage
from ao3tools.Utils import get_title_authors_from_li, get_rating_from_li

class BookmarksDownloader():

    def __init__(self, username, password, dl_folder, file_extensions=ao3_extensions, bookmarks_status=ao3_bookmarks_status, yaml_organization=None):

        self.today = date.today()

        ### Parameters ###
        self.username = username
        self.password = password
        self.dl_folder = dl_folder
        self.file_extensions = file_extensions
        self.bookmarks_status = bookmarks_status
        self.yaml_organization = yaml_organization

        self.check_file_extensions()
        self.check_bookmarks_status()

        ## Checking folder
        if not self.dl_folder.endswith('/'):
            self.dl_folder += "/"
        if not os.path.exists(self.dl_folder):
            raise FileNotFoundError("This folder does not exist: %s" % self.dl_folder)
        
        ## Loading folder architecture
        self.folder_architecture = self.get_folder_architecture()

        ## Loading id of works that were already downloaded
        self.previous_works = []
        # Path to new csv
        self.dl_works_csv = self.dl_folder + "works.csv"
        if os.path.exists(self.dl_works_csv) and os.stat(self.dl_works_csv).st_size > 0:
            self.previous_works = pd.read_csv(self.dl_works_csv, delimiter=";", index_col="id")
        else:
            self.previous_works = pd.DataFrame([], columns=["id", "title", "authors", "extensions", "date"])
            self.previous_works.set_index("id", inplace=True)

        self.sess = self.open_session()

        self.bookmarks_url = ao3BookmarksURL % self.username

    def read_bookmarks(self, header_info=""):
        """Reads the user's bookmarks and downloads the works.
        Doesn't download works whose id are in the dl_works_csv csv file that contains already downloaded works.
        Saves new ids in the dl_works_csv csv file."""
               
        # Retrieving number of pages
        lastPage = 0
        lastPage = get_lastPage(self.sess, self.bookmarks_url, header_info)

        # Going through the pages of the bookmarks
        for pageIndex in range(1, lastPage + 1):
            print("Page: %i" % pageIndex)

            soup = get_soup(self.sess, self.bookmarks_url + "?page=" + str(pageIndex), header_info)
            
            groups = soup.select("li.bookmark.blurb.group")

            # Going through the works on the page
            for groupSoup in groups:

                # Retrieving id #
                group_class = groupSoup.get("class")

                # Checking if the work wasn't deleted or hidden #
                if (groupSoup.find("div", class_="mystery header picture module")) :
                    print("this work was HIDDEN.")
                    continue

                # Getting id #
                group_class_id = group_class[3].split("-")
                group_type, group_id = group_class_id[0], group_class_id[1]

                # Checking if the work was deleted #
                if group_type == "user":
                    print("lmao this was deleted.")
                    continue

                if group_type == "series":
                    works_id = self.read_series_bookmark(group_id, header_info)
                if group_type == "work":
                    works_id = [group_id]

                # Retreiving type of bookmarks #
                status = groupSoup.find("p", class_="status")
                status = status.find("span")
                status = status["class"][0]

                # Retrieving date of last update
                last_update = groupSoup.find("div", class_="header module")
                last_update = last_update.find("p", class_="datetime").text
                last_update = datetime.strptime(last_update, "%d %b %Y")
                
                # Determining if this work needs to be downloaded
                will_dl = False
                extensions_to_dl = self.file_extensions
                if int(group_id) not in self.previous_works.index.to_list():
                    will_dl = True
                else:
                    # If the id already appears in the previous works
                    previous_group = self.previous_works.loc[int(group_id)]

                    # Checking if the work need to be reload (if updated)
                    # Case 1: several occurences
                    if type(previous_group).__name__ == "DataFrame":
                        for index, row in previous_group.iterrows():
                            row_date = datetime.strptime(row["date"], "%d/%m/%Y")
                            if last_update > row_date:
                                will_dl = True
                                break

                    # Case 2: only one occurence
                    elif type(previous_group).__name__ == "Series":
                        previous_date = datetime.strptime(previous_group["date"], "%d/%m/%Y")
                        if last_update > previous_date:
                            will_dl = True

                    # Case 3: no occurence
                    else:
                        will_dl = True

                # Checking extensions #
                if not will_dl:
                    # Case 1: several occurences
                    if type(previous_group).__name__ == "DataFrame":
                        previous_group_extension = []
                        for index, row in previous_group.iterrows():
                            previous_group_extension += row["extensions"].split(",")
                        previous_group_extension = list(set(previous_group_extension))
                    # Case 2: only one occurence
                    elif type(previous_group).__name__ == "Series":
                        previous_group_extension = previous_group["extensions"].split(",")
                    # Case 3: no occurence
                    else:
                        previous_group_extension = []

                    extensions_to_dl = []
                    for extension in self.file_extensions:
                        if extension not in previous_group_extension:
                            extensions_to_dl.append(extension)
                    if extensions_to_dl:
                        will_dl = True

                if will_dl and status in self.bookmarks_status:
                    # Downloading works
                    for work_id in works_id:
                        try:
                            self.download_work_from_id(work_id, extensions_to_dl)
                            has_dl = True
                        except ChunkedEncodingError as e:
                            print("Error: %s couldn't be downloaded (probably because it has TOO MUCH WORDS)" % work_id)
                            has_dl = False
                            continue
                    # Updating list of downloaded id
                    # Getting title and authors
                    title, authors = get_title_authors_from_li(groupSoup)
                    rating = get_rating_from_li(groupSoup)

                    if has_dl:
                        if int(group_id) not in self.previous_works.index.to_list():
                            # Case 1: only adding one line (new work)
                            with open(self.dl_works_csv, 'a', newline='') as f_dl_works:
                                writer_dl_works = writer(f_dl_works, delimiter=";")
                                if not os.stat(self.dl_works_csv).st_size > 0:
                                    writer_dl_works.writerow(["id", "type", "rating", "title", "authors", "extensions", "date"])
                                writer_dl_works.writerow([group_id, group_type, rating, title, ",".join(authors), ",".join(self.file_extensions), datetime.strftime(self.today, "%d/%m/%Y")])
                                f_dl_works.close()
                        else:
                            # Case 2: updating one line (new extension)
                            self.previous_works.loc[int(group_id)]["extensions"] = ",".join(self.file_extensions)
                            self.previous_works.to_csv(self.dl_works_csv, mode='w', index="id", header=True, sep=";")
            pageIndex += 1


    def open_session(self):
        """Opens a new session and loging to ao3."""

        # Session #
        self.sess = requests.Session()
        
        # Getting the authenticity token
        req = self.sess.get(ao3URL)
        soup = BeautifulSoup(req.text, features="html.parser")
        authenticity_token = soup.find("input", {"name": "authenticity_token"})["value"]

        # Loging to ao3
        req = self.sess.post(ao3LoginURL, params={
                "authenticity_token": authenticity_token,
                "user[login]": self.username,
                "user[password]": self.password,
            },
        )

        # Unfortunately AO3 doesn't use HTTP status codes to communicate
        # results -- it's a 200 even if the login fails.
        if "Please try again" in req.text:
            raise RuntimeError(
                "Error logging into AO3; is your password correct?")
        
        if "Session Expired" in req.text:
            raise RuntimeError(
                "Error logging into AO3; session expired")
        
        return self.sess

    def create_folder_rec(self, soup, folder, archi_key, archi_dict):
        """Creates the folder that will contains the downloaded works and returns the path to that folder."""
        type_tag = archi_dict["type"]
        archi_tags = archi_dict["tags"]
        try:
            tag_list = [unidecode(tag.text) for tag in soup.find("dd", class_="%s tags" % type_tag).find_all(class_="tag")]
            if tag_list:
                for tag in tag_list:
                # Checking if it's in the registered tags (check Variables.py)
                    if tag in archi_tags:
                        # Updating folder path
                        folder += archi_key + "/"
                        # Creating the folder if it doesn't exist
                        for file_extension in self.file_extensions:
                            if not os.path.exists(folder % file_extension):
                                os.mkdir(folder % file_extension)
        except AttributeError:
            return folder

        if not archi_dict["children"]:
            return folder

        children = archi_dict["children"]
        for childKey in children:
            folder = self.create_folder_rec(soup, folder, childKey, children[childKey])

        return folder

    def create_folder(self, soup):
        """Creates the folder that will contains the downloaded work and returns the path to that folder."""
        ## File extention
        folder = self.dl_folder + "%s/"
        # Creating the folder if it doesn't exist
        for file_extension in self.file_extensions:
            if not os.path.exists(folder % file_extension):
                os.mkdir(folder % file_extension)

        ## Case where the user doesn't wish to have the folder organized in multiples one
        if not self.folder_architecture:
            return folder

        ## Fandom: the works will be sorted into different folders corresponding to their fandoms
        # Retrieving fandoms
        fandoms_list = [unidecode(tag.text) for tag in soup.find("dd", class_="fandom tags").find_all(class_="tag")]
        # In case the work doesn't belong to a registered fandom (check Variables.py)
        fandoms_folder = "Other fandoms"
        # Checking if the first fandom is registered
        for fandomKey in self.folder_architecture.keys():
                if fandoms_list[0] in self.folder_architecture[fandomKey]["tags"]:
                    fandoms_folder = fandomKey
                    break
        # Checking if all the other fandoms are in the same group as the first (check Variables.py)
        if fandoms_folder not in ["Other fandoms"]:
            if len(fandoms_list) > 1:  
                for fandom in fandoms_list:
                    # If all the fandoms aren't resgistered to be in the same group, then it's a crossover
                    if fandom not in self.folder_architecture[fandoms_folder]["tags"]:
                        fandoms_folder = "Crossover"
                        break
        # Updating folder path
        folder += fandoms_folder + "/"
        # Creating the folder if it doesn't exist
        for file_extension in self.file_extensions:
            if not os.path.exists(folder % file_extension):
                os.mkdir(folder % file_extension)

        ## Other tags
        if fandoms_folder not in ["Other fandoms", "Crossover"]:
            children = self.folder_architecture[fandoms_folder]["children"]
            if children:
                for childKey in children:
                    previous_folder = folder
                    folder = self.create_folder_rec(soup, folder, childKey, children[childKey])

                    # If a folder was found, no need to find another one
                    # Folders in foldrs_architecture are ordered by priority (check Variables.py)
                    if folder != previous_folder:
                        break

        return folder

    def download_work_from_url(self, a_download, work_id, work_title, work_rating, folder, extensions_to_dl):
        """Downloads the work and saves it into the corresponding folder."""

        for a in a_download:
            if a.text in extensions_to_dl:
                while True:
                    try:
                        r = requests.get(ao3DownloadURL % a["href"], stream=True)
                        r.raise_for_status()
                        title = "%s_%s_%s" % (work_rating, work_id, work_title) + "." + a.text.lower()
                        with open(folder % a.text + title[:75], 'wb') as f:
                            for chunk in r.iter_content(chunk_size=8192):
                                f.write(chunk)
                        break
                    except requests.exceptions.HTTPError:
                        print("Wait...")
                        time.sleep(RETRY_LATER_DELAY)

    def download_work_from_id(self, work_id, extensions_to_dl, header_info=""):
        """Dowloads the work from its id.
        Creates the folder that will contained the downloaded work."""

        # URL of work
        url = ao3WorkURL % work_id
        #view_adult: if it"s false then a warning appears
        #view_full_work: to get the whole work in one page instead of one by chapter

        ## Scraping
        print("Downloading work: %s" % work_id)
        # Retrieving soup
        soup = get_soup(requests, url, header_info)
        # Case if loging is necessary
        if "This work is only available to registered users of the Archive." in soup.text.strip():
            soup = get_soup(self.sess, url, header_info)
        # Retrieving title (for dl name)
        work_title = unidecode(soup.find("h2", class_="title heading").text).strip()
        work_title = re.sub('[^A-Za-z0-9\s]+', '', work_title)
        # Retrieving rating (for dl name)
        work_rating = soup.find("dd", class_="rating tags").find(class_="tag").text
        work_rating = unidecode(work_rating)
        work_rating = work_rating[0]

        # Retrieving download links from download button
        metadata = soup.find("ul", class_="work navigation actions")
        li_download = metadata.find("li", class_="download")
        ul_download = li_download.find("ul", "expandable secondary")
        a_download = ul_download.find_all("a", href=True)

        ## Creating the folder that will contains the downloaded fic
        folder = self.create_folder(soup)
        ## Downloading work
        self.download_work_from_url(a_download, work_id, work_title, work_rating, folder, extensions_to_dl)

    def check_file_extensions(self):
        """Checks if the file extentions are valid."""

        if not self.file_extensions:
            raise ValueError("file_extensions cannot be empty.")
        for f_type in self.file_extensions:
            if f_type not in ao3_extensions:
                raise ValueError("%s isn't a valid file type." % f_type)

    def check_bookmarks_status(self):
        """Checks if the bookmark status are valid."""

        if not self.bookmarks_status:
            raise ValueError("bookmarks status cannot be empty.")
        for b_status in self.bookmarks_status:
            if b_status not in ao3_bookmarks_status:
                raise ValueError("%s isn't a valid bookmark status." % b_status)

    def get_folder_architecture(self):
        """Loads the yaml and returns its content."""

        if self.yaml_organization:
            with open(self.yaml_organization,  encoding="utf8") as f:
                data = yaml.load(f, Loader=SafeLoader)
        else:
            data = { }
        return data

    def read_series_bookmark(self, series_id, header_info=""):
        """Returns the ids of the works in the series."""
        
        print("Downloading series: %s" % series_id)

        ## Series URL
        url = ao3SeriesURL % series_id
        
        # Retrieving number of pages
        lastPage = 0
        lastPage = get_lastPage(self.sess, url, header_info)

        works_id = []
        # Going through the pages of the series
        for pageIndex in range(1, lastPage + 1):
            print("\tPage: %i" % pageIndex)

            soup = get_soup(self.sess, url + "?page=" + str(pageIndex), header_info)
            
            works = soup.select("li.work.blurb.group")
            
            # Going through the works on the page
            for workSoup in works:

                # Retrieving id #
                group_class = workSoup.get("class")

                # Checking if the work was deleted #
                if not group_class:
                    continue

                # Checking if the work wasn't deleted or hidden #
                if (workSoup.find("div", class_="mystery header picture module")) :
                    continue

                # Retrieving id #
                work_id = group_class[3].split("-")[1]
                print("\tWork: %s" % work_id)

                works_id.append(work_id)

        return works_id