import time

from bs4 import BeautifulSoup
from unidecode import unidecode

from ao3tools.Variables import RETRY_LATER_DELAY

def is_soup(workSoup):
    """Checks if workSoup is valid."""
    # Case if "retry later" page
    if workSoup.text.strip() == "Retry later":
        return False
    return True

def get_soup(sess, url, header_info=""):
    """Returns soup of the page located at url.
    Waits when there's an error and retries again (error 503)"""
    headers = {"user-agent" : header_info}
    req = sess.get(url, headers=headers)
    src = req.text
    soup = BeautifulSoup(src, "html.parser")
    # If there are too many request on the website, AO3 just show a 'retry later' page
    while not is_soup(soup):
        print("Wait....")
        time.sleep(RETRY_LATER_DELAY)
        headers = {"user-agent" : header_info}
        req = sess.get(url, headers=headers)
        src = req.text
        soup = BeautifulSoup(src, "html.parser")
    return soup

def get_lastPage(sess, url, header_info=""):
    """Returns the number of the last page from the pagination navigation."""
    
    soup = get_soup(sess, url, header_info)
    pagination_actions = (soup.find("ol", class_="pagination actions"))
    if not pagination_actions:
        lastPage = 1
    else:
        pagination = [unidecode(page.text) for page in pagination_actions]
        pagination = [page.strip() for page in pagination if page.strip()]
        lastPage = int(pagination[-2])
    return lastPage

def get_title_authors_from_li(metadata):
    """Scraps title and authors of a work from a browsing page (history, bookmarks...) and returns them."""
    heading = metadata.find("h4", class_="heading").find_all("a")
    heading = [unidecode(head.text) for head in heading]
    title = heading[0]
    authors = heading[1:]
    return title, authors

def get_rating_from_li(metadata):
    """Scraps rating of a work from a browsing page (history, bookmarks...) and returns it."""
    heading = metadata.find("ul", class_="required-tags").find_all("span", class_="text")
    symbols_list = [unidecode(head.text) for head in heading]
    rating = symbols_list[0]
    return rating