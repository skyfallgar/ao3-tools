# Ao3 Tools Package

## Description

This package's purpose is to provide tools for web scraping the website [AO3](archiveofourown.org).

## Installation

### Building the package by yourself

Install build if you still haven't:

```bash
pip install build
```

Build the ao3tools package:

```bash
python -m build
```

Install the ao3tools package:

```bash
pip install ./dist/ao3tools-0.0.1.tar.gz
```

### Using artefacts

![](https://i.imgur.com/ZFdH6tB.png)

You can download the *build_package* zip file, extract its content and install the package the same way.

```bash
pip install ./dist/ao3tools-0.0.1.tar.gz
```

### Commands

Once the package is installed, you can use the commands from the *commands* folder. Find documentation on how to use them in the [wiki](https://gitlab.com/skyfallgar/ao3-tools/-/wikis/home).

## Roadmap

- Expand the bookmarks module
- Create an history module
- Create an author module


## License

This work is licenced under the CC BY-NC-SA.

___
> **CC BY-NC-SA:** This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format for noncommercial purposes only, and only so long as attribution is given to the creator. If you remix, adapt, or build upon the material, you must license the modified material under identical terms. ([CreativeCommons](https://creativecommons.org/about/cclicenses/))
___

## Credits

As I didn't know how to scrap before, I took inspiration from this [AO3 Scraper](https://github.com/radiolarian/AO3Scraper) by [radiolarian](https://github.com/radiolarian). Check their work out!

## Project status

I am trying to expend this project as much as I can, but advancement rate may depends on my schedule and motivation!